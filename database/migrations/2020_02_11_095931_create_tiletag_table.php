<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiletagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tiletag', function(Blueprint $table)
		{
			$table->integer('ID', true);
			$table->integer('tagID')->index('tagID');
			$table->integer('tuileID')->index('tileID');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tiletag');
	}

}
