<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTiletagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tiletag', function(Blueprint $table)
		{
			$table->foreign('tagID', 'tagID')->references('ID')->on('tags')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('tuileID', 'tileID')->references('ID')->on('tuiles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tiletag', function(Blueprint $table)
		{
			$table->dropForeign('tagID');
			$table->dropForeign('tileID');
		});
	}

}
