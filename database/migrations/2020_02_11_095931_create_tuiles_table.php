<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tuiles', function(Blueprint $table)
		{
			$table->integer('ID', true);
			$table->integer('categoryID')->index('categoryID');
			$table->string('nom', 60);
			$table->text('Description');
			$table->float('prix', 10, 0);
			$table->integer('setupCost');
			$table->string('Prerequis')->nullable();
			$table->string('Type', 30)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tuiles');
	}

}
