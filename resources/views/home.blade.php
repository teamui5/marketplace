@extends('layouts.app')

@section('content')
    <nav class="customNav">
        <a href="#section-1">Dernière tuiles</a>
        <a href="#section-2">Catégories</a>
        {{--        <a href="#section-3">Par etiquettes</a>--}}
    </nav>
    <section class="homeSection section1" id="section-1">
        <div class="col-lg-12 hometext">
            <div class="row">
                <div><strong id="fiori">SYN'PLE </strong></div>
                <div><strong id="marketplace"> MARKETPLACE</strong></div>
            </div>
            <br>
            <div class="row">
                <a href="#section-2">
                    <button class="btnMain centerCss">Nos tuiles</button>
                </a>
            </div>
        </div>
    </section>

    <section class="homeSection section2 align-middle" id="section-2">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4 homeTitle"><h2>Nos tuiles :</h2></div>
            <div class="col-lg-4"></div>
        </div>
        <br>


        <categories id="categories"></categories>


    </section>
@endsection
