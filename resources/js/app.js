/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./smoothScroll');
require('./dropzone');
import Quill from 'quill';
import { ImageDrop } from 'quill-image-drop-module';

window.Vue = require('vue');
import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('Carousel', require('./components/Carousel.vue').default);
Vue.component('Categories', require('./components/Categories.vue').default);
Vue.component('CategoryCreateForm', require('./components/CategoryCreateForm.vue').default);
Vue.component('GestionTuiles', require('./components/GestionTuiles.vue').default);
Vue.component('Tuile', require('./components/Tuile.vue').default);
Vue.component('SearchBar', require('./components/SearchBar.vue').default);
Vue.component('unauthorized', require('./components/unauthorized.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});



const imgContent = document.querySelectorAll('.img-content-hover');

function showImgContent(e) {

    for(var i = 0; i < imgContent.length; i++) {
        imgContent[i].style.left = e.pageX + 'px';
        imgContent[i].style.top = e.pageY + 'px';
    }
}

document.addEventListener('mousemove', showImgContent);




