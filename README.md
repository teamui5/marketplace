# Syn'ple MarketPlace

MarketPlace  de tuile fiori produite par Syn'ple.

---

## Installation

### 1 - Créer un environement:


```
APP_ENV=local
APP_DEBUG=true
APP_KEY=oJiCzJoPu9FvTjmTz1jpsNHVSBL7i1nl
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
```
### 2 - Optimiser Composer
```
composer install --optimize-autoloader --no-dev
```

### 3 - Optimiser la configuration de laravel

```
php artisan config:cache
```

### 4 - Optimiser les routes

```
php artisan route:cache
```

## Les Pages

### Home :
![Home](/storage/app/public/readmeImgs/home.png)

___

Affichage des tuiles par nom, tag ou catégorie et navigation vers page de tuiles.

![Home](/storage/app/public/readmeImgs/home2.png)

#### Les composants utilisés:

Vues :
```
- app.blade.php
- home.blade.php
```
Composants vue.js :
```
- SearchBar.vue
- Categories.vue
```
Controllers :
```
- TuileController.php
- CategoriesController.php
```

### Gestion des tuiles :
![Home](/storage/app/public/readmeImgs/GestionTuile.png)

___

CRUD pour les tuiles, gestion des images des headers et des tags.

![Home](/storage/app/public/readmeImgs/crud1.png)
___
![Home](/storage/app/public/readmeImgs/crud2.png)
___
![Home](/storage/app/public/readmeImgs/crud3.png)


#### Les composants utilisés:


Vues :
```
- app.blade.php
```
Composants vue.js :
```
- SearchBar.vue
- GestionTuiles.vue
```
Controllers :
```
- TuileController.php
- CategoriesController.php
```
### Gestion des catégories :
#### Les composants utilisés:

Vues :
```
- app.blade.php
```
Composants vue.js :
```
- SearchBar.vue
- CategoryCreateForm.vue
```
Controllers :
```
- CategoriesController.php
```

### 501 :

![Home](/storage/app/public/readmeImgs/unauthorized.png)

___

#### Les composants utilisés:

Vues :
```
- app.blade.php
- unauthorized.blade.php
```
Composants vue.js :
```
- SearchBar.vue
```
