<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Images;
use App\Tuiles;

class ImagesController extends Controller
{
    public function store(Request $request)
    {
        $image = new Images([
            'nom' => $request->get('nom'),
            'image' => $request->get('image'),
            'ordre' => $request->get('ordre'),
            'tuileId' => $request->get('tuileId'),

        ]);
        $image->save();
        return response($image, 200);
    }

    public function success(Request $request)
    {
        $tuile = Tuiles::orderBy('created_at', 'desc')->first();
        $imageName = time() . '.' . $request->file->getClientOriginalExtension();
        $path = storage_path('app/public/ImagesTuiles');
        $request->file->move($path, $imageName);

        $newImage = new Images([
            'TuileID' => $tuile->ID,
            'Chemin' => $path,
            'nom' => $imageName
        ]);
        $newImage->save();

        return response('success', 200);
    }

    public function edit(Request $request)
    {
//
        $imageName = time() . '.' . $request->file->getClientOriginalExtension();
        $path = '/storage/app/public/ImagesTuiles/';
        $realPath = storage_path('app/public/ImagesTuiles');
        $request->file->move($realPath, $imageName);
        $exitingimage = Images::where('TuileID', $request->get('ID'));
        if (isset($exitingimage)) {
            $exitingimage->delete();
        }
        $newImage = new Images([
            'TuileID' => $request->get('ID'),
            'Chemin' => $path,
            'nom' => $imageName
        ]);
        $newImage->save();

        return response('success', 200);
    }

    public function getName(Request $request)
    {
        $image = Images::where('TuileID', $request->get('ID'))->first();
        return response($image, 200);
    }

    public function getForCarousel(){
        $imgs =Images::orderBy('updated_at', 'desc')->take(2)->get();
        return response($imgs, 200);
    }
}
