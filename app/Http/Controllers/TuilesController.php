<?php

namespace App\Http\Controllers;

use App\Images;
use App\Tag;
use Illuminate\Http\Request;
use App\Tuiles;
use App\TileTag;
use Illuminate\Support\Facades\Storage;

class TuilesController extends Controller
{
    public function list()
    {
        return view('tuiles.Gestion');
    }

    public function tileView($nom)
    {
        $tuile = Tuiles::where('nom', $nom)->first();
        // $tiletags = TileTag::where('tuileID', $tuile->ID)->get();
        // $tags = [];
        // foreach ($tiletags as $tiletag) {
        //     $tag = Tag::where('ID', $tiletag->tagID)->first();
        //     array_push($tags, $tag);
        // }
        // $tuile->tags = $tags;
        $tuile->Prerequis = explode(',', $tuile->Prerequis);


        $tuile->image = Images::where('TuileID', $tuile->ID)->first();

        return view('tuiles.visu', ['tuile' => $tuile]);
    }

    public function get()
    {
        return Tuiles::all();
    }

    public function store(Request $request)
    {
        $tuile = new Tuiles([
            'categoryID' => $request->get('categoryID'),
            'nom' => $request->get('nom'),
            'Description' => $request->get('Description'),
            'prix' => $request->get('prix'),
            'setupCost' => $request->get('setupCost'),
            'Prerequis' => implode(", ", $request->get('prerequis')),
            'Type' => $request->get('type')
        ]);
        $tuile->save();
        return response('success', 200);
    }

    public function getSingleTile(Request $request)
    {
        $tuile = Tuiles::where('ID', $request->get('ID'))->first();
        return response($tuile, 200);
    }

    public function search(Request $request)
    {
        $tuiles = Tuiles::where('nom', 'like', '%' . $request->nom . '%')->get();
        return $tuiles;
    }

    public function update(Request $request)
    {
        $tuileModifiee = [
            'categoryID' => $request->get('categoryID'),
            'nom' => $request->get('nom'),
            'Description' => $request->get('Description'),
            'prix' => $request->get('prix'),
            'setupCost' => $request->get('setupCost'),
            'Prerequis' => implode(", ", $request->get('Prerequis')),
            'Type' => $request->get('Type')
        ];
        Tuiles::where('ID', $request->get('ID'))->update($tuileModifiee);
        return response($tuileModifiee, 200);
    }

    public function delete(Request $request)
    {
        Tuiles::where('ID', $request->get('ID'))->delete();
        return response('tuile supprimée', 200);
    }

    public function getByCategory(Request $request)
    {
        $tuiles = Tuiles::where('categoryID', $request->get('ID'))->get();
        return response($tuiles, 200);
    }

    public function getByTag(Request $request)
    {
        $tuiles = [];
        $tiletags = TileTag::where('tagID', $request->get('ID'))->get();
        foreach ($tiletags as $tiletag) {
            $tuile = Tuiles::where('ID', $tiletag->tuileID)->first();
            array_push($tuiles, $tuile);
        }
        return $tuiles;
    }
}
