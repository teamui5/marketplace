<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\TileTag;
use App\Tuiles;


class TagController extends Controller
{
    public function get(){
        return Tag::all();
    }
}
