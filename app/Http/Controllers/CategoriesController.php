<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorie;

class CategoriesController extends Controller
{
    public function get(){
        $categories =  Categorie::all();

        return $categories;
    }

    public function create(){
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nom'=>'required'
        ]);

        $categorie = new Categorie([
            'nom' => $request->get('nom')
        ]);
        $categorie->save();
        return redirect('home')->with('success', 'catégorie créée!');
    }

    public function byId(Request $request){
        $cat = Categorie::where('ID', $request->get('id'))->first();
        return response($cat, 200);
    }

    public function edit(Request $request){
        $updatedcat= [
          'ID' => $request->get('ID'),
          'nom' => $request->get('nom')
        ];
        Categorie::where('ID', $request->get('ID'))->update($updatedcat);
        return response('success', 200);
    }

    public function delete(Request $request){
        $cat =  Categorie::where('ID', $request->get('ID'))->delete();
        return response('sucess', 200);
    }
}
