<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Images extends Model
{
    protected $table = 'images';
    protected $fillable = ['ID', 'TuileID', 'Chemin', 'nom'];
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}
