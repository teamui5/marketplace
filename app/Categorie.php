<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categorie extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'nom'
    ];
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
