<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tuiles extends Model
{
    protected $table = 'tuiles';
    protected $fillable = [
        'nom', 'categoryID', 'Description', 'prix', 'setupCost', 'Type', 'Prerequis'
    ];
    public $timestamps = true;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function tileTags()
    {
        return $this->belongsToMany('App\TileTag');
    }

    public function images(){
        return $this->hasMany('App\Images');
    }
}
