<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TileTag extends Model
{
    protected $table = 'tileTag';
    protected $fillable = [
        'tagID', 'tuileID'
    ];
    public $timestamps = false;

    public function Tags()
    {
        return $this->hasMany('App\Tag');
    }

    public function Tuiles()
    {
        return $this->hasMany('App\Tuiles');
    }
}
