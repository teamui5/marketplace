<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//catégories
Route::get('/catGet', 'CategoriesController@get')->name('/catGet');
Route::get('/catCreate', 'CategoriesController@create')->name('/catCreate')->middleware('checkRoleAdmin');
Route::post('/catStore', 'CategoriesController@store')->name('/catStore')->middleware('checkRoleAdmin');
Route::post('/catEdit', 'CategoriesController@edit')->name('/catEdit')->middleware('checkRoleAdmin');
Route::delete('/catDelete', 'CategoriesController@delete')->name('/catDelete')->middleware('checkRoleAdmin');
Route::post('/catById', 'CategoriesController@byId')->name('/catById')->middleware('checkRoleAdmin');

//tuiles CRUD
Route::post('/tuilesCat', 'TuilesController@getByCategory')->name('/tuilesCat');
Route::get('/tuilesList', 'TuilesController@list')->name('/tuilesList')->middleware('checkRoleAdmin');
Route::get('/tuilesGet', 'TuilesController@Get')->name('/tuilesGet');
Route::post('/tuilesGetSingle', 'TuilesController@getSingleTile')->name('/tuilesGetSingle');
Route::post('/tuilesStore', 'TuilesController@store')->name('/tuilesStore')->middleware('checkRoleAdmin');
Route::put('/tuilesUpdate', 'TuilesController@update')->name('/tuilesUpdate')->middleware('checkRoleAdmin');
Route::delete('/tuilesDelete', 'TuilesController@delete')->name('/tuilesDelete')->middleware('checkRoleAdmin');
Route::post('/tuilesSearch', 'TuilesController@search')->name('/tuilesSearch');
Route::post('/tileByTag', 'TuilesController@getByTag')->name('/tileByTag');


//tuiles visu
Route::get('/tuile/{nom}', 'TuilesController@tileView')->name('/tuile/{nom}');

//images
Route::post('/imgStore', 'ImagesController@store')->name('/imgStore')->middleware('checkRoleAdmin');
Route::post('/imgSuccess', 'ImagesController@success')->name('/imgSuccess');
Route::post('/imgEdit', 'ImagesController@edit')->name('/imgEdit')->middleware('checkRoleAdmin');
Route::post('/imgGetName', 'ImagesController@getName')->name('/imgGetName');
Route::get('/imgGetCarousel', 'ImagesController@getForCarousel')->name('/imgGetCarousel');

//Tag

Route::get('/tagGet', 'TagController@get')->name('/tagGet');
